/* Main JavaScript file */

import './style.css';
import logo from './logo';

if (typeof document === 'undefined') {
  console.log('Node native initiation mode');
  console.log('Hello world!');
} else {
  console.log('Web server initiation mode');
  const selector = document.querySelector('canvas');
  logo(35, selector);
}
